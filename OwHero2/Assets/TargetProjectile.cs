﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetProjectile : MonoBehaviour
{
    private GameObject lastTarget;
    private GameObject nextTarget;

    public float lifeTime;
    public float lifeTimeLeft;

    public float aimRadius;

    public float speed;

    public bool freeFloat;
    // Start is called before the first frame update
    void Start()
    {
        FindNextTarget();
    }

    // Update is called once per frame
    void Update()
    {
        lifeTimeLeft += -Time.deltaTime;
        
        if (lifeTimeLeft <= 0) Destroy(gameObject);
        
        float step =  speed * Time.deltaTime; // calculate distance to move
        
        if (!freeFloat)
        {
            transform.position = Vector3.MoveTowards(transform.position, nextTarget.transform.position, step);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.position+ Vector3.right, step);
        }
        
    }

    bool FindNextTarget()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, aimRadius);
        
        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].CompareTag("Dummie"))
            {
                if (lastTarget)
                {
                    if (hitColliders[i].gameObject.GetInstanceID() != lastTarget.GetInstanceID())
                    {
                        nextTarget = hitColliders[i].gameObject;
                        return true; 
                    }
                }
                else
                {
                    nextTarget = hitColliders[i].gameObject;
                    return true; 
                }
            }
            i++;
        }

        nextTarget = null;
        return false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (lastTarget)
        {
            if (other.CompareTag("Dummie") & other.gameObject.GetInstanceID() != lastTarget.GetInstanceID()) lastTarget = other.gameObject;
            freeFloat = !FindNextTarget();
        }
        else
        {
            if (other.CompareTag("Dummie")) lastTarget = other.gameObject;
            freeFloat = !FindNextTarget();
        }
        
    }
}
