﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public GameObject spawn;
    public float massIncrease;
    private bool startMassIncrease = false;
    private Rigidbody _rb;

    public bool allCollisions = false;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (startMassIncrease)
        {
            AddMass();
           
        }
    }
   
    private void OnCollisionEnter(Collision other)
    {
        foreach(ContactPoint contact in other.contacts){
            
            if (allCollisions)
            {
                Instantiate(spawn).transform.position = contact.point;
                Destroy(gameObject);
            }
            
            if(Vector2.Angle(contact.normal,Vector3.up ) < 60 ){
                //grounded = true;
                Instantiate(spawn).transform.position = contact.point;
                Destroy(gameObject);
            }

            
        }

        startMassIncrease = true;
    }

    void AddMass()
    {
        //_rb.mass += massIncrease;
        //_rb.velocity.y
        _rb.AddForce(Vector3.down*massIncrease);
    }
}
