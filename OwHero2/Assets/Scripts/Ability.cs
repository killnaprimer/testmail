﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ability : MonoBehaviour
{
    
 
    
    public Camera cam;
    RaycastHit hit;
    
    
    [Header("Shift Params")]
    public float maxDistance = 1000000;
    public float shiftCooldown;
    public LayerMask ignoreLayer;
    public GameObject joint;
    public GameObject targetMarker;
    public float jumpImpulse = 100000f;
    public Text shiftCooldownText;   
    
    [Header("E Params")]
    public float eCooldown;
    public GameObject e_grenade;
    public float grenaImpulse;
    public Text eCooldownText;    
    
    [Header("Q Params")]
    public GameObject ultGrenade;
    public Text ultCooldownText; 
    public float ultCooldown;
    
    private float shiftCurrentCooldown;
    private float eCurrentCooldown;
    private float ultCurrentCooldown;
    private IEnumerator coroutine;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
       if (!joint.activeSelf&(shiftCurrentCooldown > 0)) shiftCurrentCooldown += -Time.deltaTime;
       if (eCurrentCooldown > 0) eCurrentCooldown += -Time.deltaTime;
       if (ultCurrentCooldown > 0) ultCurrentCooldown += -Time.deltaTime;

       if (Input.GetKey(KeyCode.LeftShift) & shiftCurrentCooldown <= 0)
       {
           if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, maxDistance,
               ~ignoreLayer))
           {
               targetMarker.SetActive(true);
               targetMarker.transform.position = hit.point;
           }
           else
           {
               targetMarker.SetActive(false);
           }
       }
       
       if (Input.GetKeyUp(KeyCode.LeftShift ) & shiftCurrentCooldown <= 0)
       {
            Glue();
            
       }
        
       if (Input.GetKeyDown(KeyCode.E)){
            if (eCurrentCooldown <= 0)
            {
                ThrowPuddle();
                eCurrentCooldown = eCooldown;
            }
       }

       if (Input.GetKeyDown(KeyCode.Q))
       {
           if (ultCurrentCooldown <= 0)
           {
               StartCoroutine(ThrowUlt(0.25f));
               ultCurrentCooldown = ultCooldown;
           }
           
       }

    }
    
    
    void ThrowPuddle()
    {
        var grenade = Instantiate(e_grenade);
        grenade.transform.position = cam.transform.position + cam.transform.TransformDirection(Vector3.forward);
        grenade.GetComponent<Rigidbody>().AddForce(cam.transform.forward * (grenaImpulse+grenaImpulse*GetHorizontLine()) );
    }

    void Glue()
    {
        if (!joint.activeSelf  )
        {
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, maxDistance,
                ~ignoreLayer))
            {
                joint.transform.position = hit.point;
                joint.SetActive(true);
                targetMarker.SetActive(false);
            }
        }
        else
        {
                
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 250,
                ~ignoreLayer))
            {
                joint.SetActive(false);
                    
                var heading = hit.point - transform.position;
                GetComponent<PlayerMovementScript>().ignoreSpeedLimit = true;
                GetComponent<Rigidbody>().AddForce((heading.normalized-Vector3.up) * 20000f);
                shiftCurrentCooldown = shiftCooldown;
            }
        }
    }

    void Ult()
    {
        
        var grenade = Instantiate(ultGrenade);
        grenade.transform.position = cam.transform.position + cam.transform.TransformDirection(Vector3.forward);
        grenade.GetComponent<Rigidbody>().AddForce(cam.transform.forward * (grenaImpulse+grenaImpulse*GetHorizontLine()) );
    }
        
    float GetHorizontLine()
    {
        float horizontLine = cam.transform.localRotation.x;
        return horizontLine;
    }
    
    private IEnumerator ThrowUlt(float waitTime)
    {
        yield return new WaitForSeconds(waitTime*2);
        Ult();
        yield return new WaitForSeconds(waitTime);
        Ult();
        yield return new WaitForSeconds(waitTime);
        Ult();
        yield return new WaitForSeconds(waitTime);
        Ult();
        yield return new WaitForSeconds(waitTime);
        Ult();
        yield return new WaitForSeconds(waitTime);
        
    }
    
    void UpdateUI()
    {
        if (shiftCurrentCooldown > 0)
        {
            shiftCooldownText.text = Mathf.FloorToInt(shiftCurrentCooldown).ToString();
            shiftCooldownText.color = Color.white;
        }
        else
        {
            shiftCooldownText.text = "SHIFT";
            shiftCooldownText.color = Color.yellow;
        }
       
        if (eCurrentCooldown > 0)
        {
            eCooldownText.text = Mathf.FloorToInt(eCurrentCooldown).ToString();
            eCooldownText.color = Color.white;
        }
        else
        {
            eCooldownText.text = "E";
            eCooldownText.color = Color.yellow;
        } 
        
        if (ultCurrentCooldown > 0)
        {
            ultCooldownText.text = Mathf.FloorToInt(ultCurrentCooldown).ToString();
            ultCooldownText.color = Color.white;
        }
        else
        {
            ultCooldownText.text = "E";
            ultCooldownText.color = Color.yellow;
        }
    }
}
