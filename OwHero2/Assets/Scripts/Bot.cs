﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : MonoBehaviour
{
    public int health;

    private int maxHealth;
    private float moveSpeed = 5f;
    public float slowDownPercent = 0.5f;
    public float slowDownMaxDuration = 3f;
    private NavMeshAgent _agent;
    public float slowDownCurrentDuration;

    public Transform[] targets;
    public ParticleSystem particles;
    private int currentTarget;
    
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        _agent = GetComponent<NavMeshAgent>();
        particles.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        print(_agent.speed);
        if (slowDownCurrentDuration >= 0)
        {
            slowDownCurrentDuration += -Time.deltaTime;
        }
        else
        {
            _agent.speed = moveSpeed;
            particles.gameObject.SetActive(false);
        }
        

        if (Vector3.Distance(transform.position, targets[currentTarget].position)<1f)
        {
            currentTarget++;
            if (currentTarget >= targets.Length) currentTarget = 0;
        }

        _agent.SetDestination(targets[currentTarget].position);


    }

    public void StartSlowdown()
    {
        slowDownCurrentDuration = slowDownMaxDuration;
        _agent.speed = moveSpeed * slowDownPercent;
        particles.gameObject.SetActive(true);

    }
    
}
