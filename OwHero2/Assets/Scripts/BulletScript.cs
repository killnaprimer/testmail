﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour
{
	public int numBullets;
	public float spread;
	[Tooltip("Furthest distance bullet will look for target")]
	public float maxDistance = 1000000;
	RaycastHit hit;
	[Tooltip("Prefab of wall damange hit. The object needs 'LevelPart' tag to create decal on it.")]
	public GameObject decalHitWall;
	[Tooltip("Decal will need to be sligtly infront of the wall so it doesnt cause rendeing problems so for best feel put from 0.01-0.1.")]
	public float floatInfrontOfWall;
	[Tooltip("Blood prefab particle this bullet will create upoon hitting enemy")]
	public GameObject bloodEffect;
	[Tooltip("Put Weapon layer and Player layer to ignore bullet raycast.")]
	public LayerMask ignoreLayer;
	
	/*
	* Uppon bullet creation with this script attatched,
	* bullet creates a raycast which searches for corresponding tags.
	* If raycast finds somethig it will create a decal of corresponding tag.
	*/
	void Update ()
	{
		Vector3 direction = transform.forward;
		direction.x += Random.Range(-spread, spread);
		direction.y += Random.Range(-spread, spread);
		direction.z += Random.Range(-spread, spread);
		for (int i = 0; i < numBullets; i++)
		{
			
		
		if(Physics.Raycast(transform.position, direction,out hit, maxDistance, ~ignoreLayer)){
			if(decalHitWall){
				if(hit.transform.tag == "LevelPart"){
					Instantiate(decalHitWall, hit.point + hit.normal * floatInfrontOfWall, Quaternion.LookRotation(hit.normal));
					//Destroy(gameObject);
				}
				if(hit.transform.tag == "Dummie"){
					Instantiate(bloodEffect, hit.point, Quaternion.LookRotation(hit.normal));
					//Destroy(gameObject);
				}
			}		
			//Destroy(gameObject);
		}
		}
		Destroy(gameObject, 0.1f);
	}

}
