﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectArea : MonoBehaviour
{
    public float lifetime;
    private IEnumerator coroutine;
    // Start is called before the first frame update
    void Start()
    {
        coroutine = WaitToRemove(lifetime);
        StartCoroutine(coroutine);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private IEnumerator WaitToRemove(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dummie")) other.gameObject.SendMessage("StartSlowdown");
    }
}
